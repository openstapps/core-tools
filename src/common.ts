/*
 * Copyright (C) 2018-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {existsSync, mkdir, readFile, unlink, writeFile} from 'fs';
import {Glob} from 'glob';
import {platform} from 'os';
import {promisify} from 'util';
import path from 'path';

export const globPromisified = promisify(Glob);
export const mkdirPromisified = promisify(mkdir);
export const readFilePromisified = promisify(readFile);
export const writeFilePromisified = promisify(writeFile);
export const unlinkPromisified = promisify(unlink);

/**
 * Get path that contains a tsconfig.json
 *
 * @param startPath Path from where to start searching "upwards"
 */
export function getTsconfigPath(startPath: string): string {
  let tsconfigPath = startPath;

  // see https://stackoverflow.com/questions/9652043/identifying-the-file-system-root-with-node-js
  const root = platform() === 'win32' ? process.cwd().split(path.sep)[0] : '/';

  // repeat until a tsconfig.json is found
  while (!existsSync(path.join(tsconfigPath, 'tsconfig.json'))) {
    if (tsconfigPath === root) {
      throw new Error(
        `Reached file system root ${root} while searching for 'tsconfig.json' in ${startPath}!`,
      );
    }

    // pop last directory
    const tsconfigPathParts = tsconfigPath.split(path.sep);
    tsconfigPathParts.pop();
    tsconfigPath = tsconfigPathParts.join(path.sep);
  }

  Logger.info(`Using 'tsconfig.json' from ${tsconfigPath}.`);

  return tsconfigPath;
}
