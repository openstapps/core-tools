/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {readdirSync, statSync} from 'fs';
import {flatMap} from 'lodash';
import path from 'path';

/**
 * Expand a path to a list of all files deeply contained in it
 */
export function expandPathToFilesSync(sourcePath: string, accept: (fileName: string) => boolean): string[] {
  const fullPath = path.resolve(sourcePath);
  const directory = statSync(fullPath);

  return directory.isDirectory()
    ? flatMap(readdirSync(fullPath), fragment =>
        expandPathToFilesSync(path.resolve(sourcePath, fragment), accept),
      )
    : [fullPath].filter(accept);
}

/**
 * Take a Windows path and make a Unix path out of it
 */
export function toUnixPath(pathString: string): string {
  return pathString.replace(/\\/g, '/');
}
