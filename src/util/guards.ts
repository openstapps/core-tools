/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {JSONSchema7 as JSONSchema} from 'json-schema';
import {SchemaWithDefinitions} from '../types/schema';

/**
 * Guard for if a JSON schema is in fact a schema with definitions
 */
export function isSchemaWithDefinitions(schema: JSONSchema): schema is SchemaWithDefinitions {
  return schema.definitions !== undefined;
}

/**
 * Guard method for determining if an object (a thing) has a type property with a type of string
 */
export function isThingWithType(thing: unknown): thing is {type: string} {
  return (
    typeof thing === 'object' &&
    thing !== null &&
    'type' in thing &&
    typeof (thing as {type: unknown}).type === 'string'
  );
}
