/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import path from 'path';
import {PathLike} from 'fs';

/**
 * Convert a path to a POSIX path
 *
 * Replaces the system separator with posix separators
 * Replaces Windows drive letters with a root '/'
 */
export function toPosixPath(pathLike: PathLike, separator = path.sep): string {
  return pathLike
    .toString()
    .split(separator)
    .join(path.posix.sep)
    .replace(/^[A-z]:\//, '/');
}
