/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {omitBy, isNil, reject, isEmpty, isArray, isObject} from 'lodash';

/**
 * Filters only defined elements
 */
export function rejectNil<T>(array: Array<T | undefined | null>): T[] {
  return reject(array, isNil) as T[];
}

/**
 * Map elements that are not null
 */
export function mapNotNil<T, S>(array: readonly T[], transform: (element: T) => S | undefined | null): S[] {
  return rejectNil(array.map(transform));
}

/**
 * Deletes all properties with the value 'undefined', [] or {}
 */
// eslint-disable-next-line @typescript-eslint/ban-types
export function cleanupEmpty<T extends object>(object: T): T {
  return omitBy(object, it => isNil(it) || ((isObject(it) || isArray(it)) && isEmpty(it))) as T;
}
