/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {first, last, tail, filter} from 'lodash';
import {
  ArrayTypeNode,
  ClassDeclaration,
  ClassElement,
  EnumDeclaration,
  Identifier,
  InterfaceDeclaration,
  isArrayTypeNode,
  isClassDeclaration,
  isComputedPropertyName,
  isEnumDeclaration,
  isInterfaceDeclaration,
  isPropertyDeclaration,
  isPropertySignature,
  isTypeAliasDeclaration,
  isTypeReferenceNode,
  NodeArray,
  PropertyDeclaration,
  PropertyName,
  PropertySignature,
  TypeAliasDeclaration,
  TypeElement,
  TypeNode,
  TypeReferenceNode,
} from 'typescript';
import * as ts from 'typescript';
import {cleanupEmpty} from '../util/collections';
import {LightweightComment} from './types/lightweight-comment';

/** @internal */
export function extractComment(node: ts.Node): LightweightComment | undefined {
  const jsDocument = last(
    // @ts-expect-error jsDoc exists in reality
    node.jsDoc as
      | Array<{
          comment?: string;
          tags?: Array<{comment?: string; tagName?: {escapedText?: string}}>;
        }>
      | undefined,
  );
  const comment = jsDocument?.comment?.split('\n\n');

  return jsDocument === undefined
    ? undefined
    : cleanupEmpty({
        shortSummary: first(comment),
        description: tail(comment)?.join('\n\n'),
        tags: jsDocument?.tags?.map(tag =>
          cleanupEmpty({
            name: tag.tagName?.escapedText ?? 'UNRESOLVED_NAME',
            parameters: tag.comment?.split(' '),
          }),
        ),
      });
}

/** @internal */
export function isProperty(
  node: ClassElement | TypeElement,
): node is PropertyDeclaration | PropertySignature {
  return isPropertyDeclaration(node) || isPropertySignature(node);
}

/** @internal */
export function filterNodeTo<T extends ts.Node, S extends T>(
  node: NodeArray<T>,
  check: (node: T) => node is S,
): S[] {
  return filter(node, check);
}

/** @internal */
export function filterChildrenTo<T extends ts.Node>(node: ts.Node, check: (node: ts.Node) => node is T): T[] {
  const out: T[] = [];
  node.forEachChild(child => {
    if (check(child)) {
      out.push(child);
    }
  });

  return out;
}

/** @internal */
export function getModifiers(text: string, kind: string): string[] {
  return [
    ...text
      .split(kind)[0]
      .split(/\s+/)
      .filter(it => it !== ''),
    kind,
  ];
}

/** @internal */
export function resolvePropertyName(name?: PropertyName): string | undefined {
  return name === undefined
    ? undefined
    : isComputedPropertyName(name)
    ? 'UNSUPPORTED_IDENTIFIER_TYPE'
    : name.getText();
}

/** @internal */
export function resolveTypeName(type?: TypeNode): string | undefined {
  // @ts-expect-error typeName exists in reality
  return type?.typeName?.escapedText ?? type?.typeName?.right?.escapedText;
}

/** @internal */
export function isArrayLikeType(typeNode?: TypeNode): typeNode is ArrayTypeNode | TypeReferenceNode {
  return typeNode !== undefined && (isArrayTypeNode(typeNode) || isArrayReference(typeNode));
}

/** @internal */
export function isArrayReference(typeNode: TypeNode): boolean {
  return isTypeReferenceNode(typeNode) && (typeNode.typeName as Identifier).escapedText === 'Array';
}

/** @internal */
export function isClassLikeNode(node: ts.Node): node is ClassDeclaration | InterfaceDeclaration {
  return isClassDeclaration(node) || isInterfaceDeclaration(node);
}

/** @internal */
export function isEnumLikeNode(node: ts.Node): node is EnumDeclaration | TypeAliasDeclaration {
  return isEnumDeclaration(node) || isTypeAliasDeclaration(node);
}
