/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {LightweightDefinitionBase} from './lightweight-definition';
import {LightweightDefinitionKind} from './lightweight-definition-kind';
import {LightweightIndexSignature, LightweightProperty} from './lightweight-property';
import {LightweightType} from './lightweight-type';

/**
 * Represents a class definition
 */
export interface LightweightClassDefinition extends LightweightDefinitionBase {
  /**
   * String values of the extended definitions
   */
  extendedDefinitions?: LightweightType[];

  /**
   * String values of the implemented definitions
   */
  implementedDefinitions?: LightweightType[];

  /**
   * Index signatures
   */
  indexSignatures?: Record<string, LightweightIndexSignature>;

  /**
   * Kind
   */
  kind: LightweightDefinitionKind.CLASS_LIKE;

  /**
   * Properties of the definition
   */
  properties?: Record<string, LightweightProperty>;

  /**
   * Generic type parameters of this class
   */
  typeParameters?: string[];
}
