/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {LightweightDefinitionKind} from './lightweight-definition-kind';
import {LightweightComment} from './lightweight-comment';
import {LightweightClassDefinition} from './lightweight-class-definition';
import {LightweightAliasDefinition} from './lightweight-alias-definition';

export type LightweightDefinition = LightweightClassDefinition | LightweightAliasDefinition;

/**
 * Represents any definition without specifics
 */
export interface LightweightDefinitionBase {
  /**
   * The comment of the definition
   */
  comment?: LightweightComment;

  /**
   * Kind of the definition
   */
  kind: LightweightDefinitionKind;

  /**
   * The definition type
   * e.g. [`abstract`, `class`] or [`enum`] or [`export`, `type`]
   */
  modifiers?: string[];

  /**
   * Name of the definition
   */
  name: string;
}
