/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {LightweightComment} from './lightweight-comment';
import {LightweightType} from './lightweight-type';

/**
 * Represents a property definition
 */
export interface LightweightProperty {
  /**
   * The comment of the property
   */
  comment?: LightweightComment;

  /**
   * Name of the property
   */
  name: string;

  /**
   * Is the property marked as optional
   */
  optional?: true;

  /**
   * A record of properties if the property happens to be a type literal
   */
  properties?: Record<string, LightweightProperty>;

  /**
   * Type of the property
   */
  type: LightweightType;
}

export interface LightweightIndexSignature extends LightweightProperty {
  /**
   * Type of the index signature, if it is an index signature
   */
  indexSignatureType: LightweightType;
}
