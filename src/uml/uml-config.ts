/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Holds configuration information of how the UML code should be build
 */
export interface UMLConfig {
  /**
   * Defines which definitions are shown
   */
  definitions: string[];

  /**
   * Defines the output file name without file extension
   */
  outputFileName?: string;

  /**
   * Should the associations between definitions be shown
   */
  showAssociations: boolean;

  /**
   * Should enum/-like values be shown
   */
  showEnumValues: boolean;

  /**
   * Should the inheritance be shown
   */
  showInheritance: boolean;

  /**
   * Should the inherited properties be shown
   */
  showInheritedProperties: boolean;

  /**
   * Should optional properties be shown
   */
  showOptionalProperties: boolean;

  /**
   * Should properties be shown
   */
  showProperties: boolean;
}
