import re2 from 're2';

type Re2 = typeof re2 & {code: string};
(re2 as Re2).code = 'require("lib/types/re2").default';

export default re2 as Re2;
