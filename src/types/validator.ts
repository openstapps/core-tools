/*
 * Copyright (C) 2018-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * The validation result
 */
export interface ValidationResult {
  /**
   * A list of errors that occurred
   */
  errors: ValidationError[];

  /**
   * whether the validation was successful
   */
  valid: boolean;
}

/**
 * An error that occurred while validating
 *
 * This is a duplicate of the ValidationError in core/protocol/errors/validation because of incompatibilities
 * between TypeDoc and TypeScript
 */
export interface ValidationError {
  /**
   * JSON schema path
   */
  dataPath: string;

  /**
   * The instance
   */
  instance: unknown;

  /**
   * The message
   *
   * Provided by https://www.npmjs.com/package/better-ajv-errors
   */
  message: string;

  /**
   * Name of the error
   */
  name: string;

  /**
   * Path within the Schema
   */
  schemaPath: string;

  /**
   * Suggestion to fix the occurring error
   *
   * Provided by https://www.npmjs.com/package/better-ajv-errors
   */
  suggestion?: string;
}

/**
 * An expected error
 */
export interface ExpectedValidationError extends ValidationError {
  /**
   * Whether or not the error is expected
   */
  expected: boolean;
}

/**
 * A map of files and their expected validation errors
 */
export interface ExpectedValidationErrors {
  [fileName: string]: ExpectedValidationError[];
}
