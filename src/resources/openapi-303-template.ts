/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {OpenAPIV3} from 'openapi-types';

export const openapi3Template: OpenAPIV3.Document = {
    openapi: '3.0.3',
    info: {
      title: 'Openstapps Backend',
      description: `# Introduction
This is a human readable documentation of the backend OpenAPI representation.`,
      contact: {
        name: 'Openstapps Team',
        url: 'https://gitlab.com/openstapps/backend',
        email: 'app@uni-frankfurt.de',
      },
      license: {
        name: 'AGPL 3.0',
        url: 'https://www.gnu.org/licenses/agpl-3.0.en.html',
      },
      version: '2.0.0',
    },
    servers: [
      {
        url: 'https://mobile.server.uni-frankfurt.de:3000',
        description: 'Production server',
      },
    ],
    paths: {},
  };
