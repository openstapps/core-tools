/* eslint-disable unicorn/prefer-module */
/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {expect} from 'chai';
import {existsSync, mkdirSync, writeFileSync} from 'fs';
import {JSONSchema7 as Schema} from 'json-schema';
import {slow, suite, test, timeout} from '@testdeck/mocha';
import rimraf from 'rimraf';
import {Foo} from '../src/resources/foo';
import {Converter} from '../src/schema';
import {Validator} from '../src/validate';
import path from 'path';

process.on('unhandledRejection', (error: unknown) => {
  if (error instanceof Error) {
    void Logger.error('UNHANDLED REJECTION', error.stack);
  }
  process.exit(1);
});

const tmpdir = path.join(__dirname, 'tmp');
const fooInstance: Foo = {
  lorem: 'ipsum',
  type: 'Foo',
};

@suite(timeout(40_000), slow(5000))
export class ValidateSpec {
  static converter: Converter;

  static schema: Schema;

  static before() {
    this.converter = new Converter(path.join(__dirname, '..', 'src', 'resources'));
    this.schema = this.converter.getSchema('Foo', '0.0.1');
    if (!existsSync(tmpdir)) {
      mkdirSync(tmpdir);
    }
    writeFileSync(path.join(tmpdir, 'SCFoo.json'), JSON.stringify(this.schema, undefined, 2));
  }

  static after() {
    rimraf(tmpdir, error => {
      // tslint:disable-next-line: no-unused-expression
      expect(error, `Unable to remove temporary directory for tests at: ${tmpdir}`).to.be.null;
    });
  }

  @test
  async validateBySchemaIdentifyingString() {
    const validator = new Validator();
    await validator.addSchemas(tmpdir);
    const validationResult = validator.validate(fooInstance, 'SCFoo');
    // tslint:disable-next-line: no-unused-expression
    expect(validationResult.errors, JSON.stringify(validationResult.errors, undefined, 2)).to.be.empty;
  }

  @test
  async validateBySchemaInstance() {
    const validator = new Validator();
    const validationResult = validator.validate(fooInstance, ValidateSpec.schema);
    // tslint:disable-next-line: no-unused-expression
    expect(validationResult.errors, JSON.stringify(validationResult.errors, undefined, 2)).to.be.empty;
  }

  @test
  async validateIntrinsic() {
    const validator = new Validator();
    await validator.addSchemas(tmpdir);
    const validationResult = validator.validate(fooInstance);
    // tslint:disable-next-line: no-unused-expression
    expect(validationResult.errors, JSON.stringify(validationResult.errors, undefined, 2)).to.be.empty;
  }
}
