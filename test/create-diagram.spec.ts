/* eslint-disable unicorn/prefer-module */
/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {existsSync, unlinkSync} from 'fs';
import {slow, suite, test, timeout} from '@testdeck/mocha';
import {createDiagram, createDiagramFromString} from '../src/uml/create-diagram';
import {UMLConfig} from '../src/uml/uml-config';
import {LightweightDefinition} from '../src/easy-ast/types/lightweight-definition';
import nock = require('nock');
import {lightweightDefinitionsFromPath} from '../src/easy-ast/easy-ast';
import path from 'path';

@suite(timeout(15_000), slow(5000))
export class CreateDiagramSpec {
  plantUmlConfig: UMLConfig;

  definitions: LightweightDefinition[];

  constructor() {
    this.plantUmlConfig = {
      definitions: [],
      showAssociations: true,
      showEnumValues: true,
      showInheritance: true,
      showInheritedProperties: true,
      showOptionalProperties: true,
      showProperties: true,
    };

    this.definitions = lightweightDefinitionsFromPath('./test/model');
  }

  @test
  async shouldRefuseRequest() {
    const testPlantUmlCode = 'class Test{\n}';
    try {
      await createDiagramFromString(testPlantUmlCode, 'http://plantuml:8080');
    } catch (error) {
      expect([
        new Error('getaddrinfo ENOTFOUND plantuml plantuml:8080').message,
        new Error('getaddrinfo EAI_AGAIN plantuml plantuml:8080').message,
        new Error('getaddrinfo ENOTFOUND plantuml').message,
      ]).to.include((error as NodeJS.ErrnoException).message);
    }
  }

  /**
   * This test will only test the functionality of the method
   * - Converting the definitions to plantuml code
   * - Sending the code to a server
   * - Writing the response to a file
   * This test will not check the file content
   */
  @test
  async shouldCreateDiagrams() {
    nock('http://plantuml:8080')
      .persist()
      .get(() => true)
      .reply(200, 'This will be the file content');

    let fileName = await createDiagram(this.definitions, this.plantUmlConfig, 'http://plantuml:8080');
    let filePath = path.resolve(__dirname, '..', fileName);
    expect(await existsSync(filePath)).to.equal(true);

    await unlinkSync(fileName);
    this.plantUmlConfig.showAssociations = false;

    this.plantUmlConfig.showInheritance = false;
    fileName = await createDiagram(this.definitions, this.plantUmlConfig, 'http://plantuml:8080');
    filePath = path.resolve(__dirname, '..', fileName);
    expect(await existsSync(filePath)).to.equal(true);
    await unlinkSync(fileName);

    nock.cleanAll();
  }
}
