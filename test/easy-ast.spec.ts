/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expandPathToFilesSync, toUnixPath} from '../src/util/io';
import {EasyAstSpecType} from './easy-ast/easy-ast-spec-type';
import {lightweightProjectFromPath} from '../src/easy-ast/easy-ast';
import {expect} from 'chai';
import {omitBy} from 'lodash';

describe('Easy AST', async () => {
  const project = lightweightProjectFromPath('./test/easy-ast', true);
  for (const file of expandPathToFilesSync('./test/easy-ast', file => file.endsWith('ast-test.ts'))) {
    try {
      const test = (await import(file))['testConfig'] as EasyAstSpecType;

      it(test.testName, () => {
        expect(omitBy(project[toUnixPath(file)], (_value, key) => key.startsWith('$'))).to.be.deep.equal(
          test.expected,
        );
      });
    } catch (error) {
      console.error(error);
    }
  }
});
