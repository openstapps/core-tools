/*
 * Copyright (C) 2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {expect} from 'chai';
import {toPosixPath} from '../src/util/posix-path';
import path from 'path';

describe('posix-path', function () {
  it('should convert Windows paths', function () {
    expect(toPosixPath('a\\b\\c', path.win32.sep)).to.be.equal('a/b/c');
  });

  it('should leave POSIX paths untouched', function () {
    expect(toPosixPath('a/b/c', path.posix.sep)).to.be.equal('a/b/c');
  });

  it('should replace Windows drive letters', function () {
    expect(toPosixPath('C:\\a\\b\\c', path.win32.sep)).to.be.equal('/a/b/c');
  });

  it('should leave POSIX root separator untouched', function () {
    expect(toPosixPath('/a/b/c', path.posix.sep)).to.be.equal('/a/b/c');
  });
});
