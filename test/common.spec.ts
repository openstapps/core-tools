/* eslint-disable unicorn/prefer-module */
/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {expect} from 'chai';
import {slow, suite, test, timeout} from '@testdeck/mocha';
import {cwd} from 'process';
import {getTsconfigPath} from '../src/common';

process.on('unhandledRejection', (reason: unknown): void => {
  if (reason instanceof Error) {
    void Logger.error('UNHANDLED REJECTION', reason.stack);
  }
  process.exit(1);
});

@suite(timeout(20_000), slow(10_000))
export class CommonSpec {
  @test
  async getTsconfigPath() {
    expect(getTsconfigPath(__dirname)).to.be.equal(cwd());
  }
}
