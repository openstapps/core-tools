/* eslint-disable unicorn/prefer-module */
/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Logger} from '@openstapps/logger';
import {expect} from 'chai';
import {slow, suite, test, timeout} from '@testdeck/mocha';
import {Converter} from '../src/schema';
import path from 'path';

process.on('unhandledRejection', (error: unknown) => {
  if (error instanceof Error) {
    void Logger.error('UNHANDLED REJECTION', error.stack);
  }
  process.exit(1);
});

@suite(timeout(40_000), slow(10_000))
export class SchemaSpec {
  @test
  async getSchema() {
    const converter = new Converter(path.join(__dirname, '..', 'src', 'resources'));

    const schema = converter.getSchema('Foo', '0.0.1');
    expect(schema).to.be.deep.equal({
      $id: 'https://core.stapps.tu-berlin.de/v0.0.1/lib/schema/Foo.json',
      $schema: 'http://json-schema.org/draft-07/schema#',
      additionalProperties: false,
      definitions: {
        FooType: {
          description: 'This is a simple type declaration for usage in the Foo interface.',
          const: 'Foo',
          type: 'string',
        },
        SCFoo: {
          additionalProperties: false,
          description:
            'This is a simple interface declaration for testing the schema generation and validation.',
          properties: {
            lorem: {
              description: 'Dummy parameter',
              enum: ['lorem', 'ipsum'],
              type: 'string',
            },
            type: {
              $ref: '#/definitions/FooType',
              description: 'String literal type property',
            },
          },
          required: ['lorem', 'type'],
          type: 'object',
        },
      },
      description: 'This is a simple interface declaration for testing the schema generation and validation.',
      properties: {
        lorem: {
          description: 'Dummy parameter',
          enum: ['lorem', 'ipsum'],
          type: 'string',
        },
        type: {
          $ref: '#/definitions/FooType',
          description: 'String literal type property',
        },
      },
      required: ['lorem', 'type'],
      type: 'object',
    });
  }
}
