/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {TestFirstUnion} from './test-union';

export class TestClass<T> {
  test2: T;

  test4: TestFirstUnion;

  constructor(type: T) {
    this.test2 = type;
    this.test4 = 'test1';
  }

  /**
   * Should not be processed at all
   */
  testClassFunction(): boolean {
    return true;
  }
}

export class TestSecondClass extends TestClass<string> {}
