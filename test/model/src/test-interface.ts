/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {TestClass, TestSecondClass} from './test-class';
import {TestFirstEnum} from './test-enum';
import {TestThirdUnion} from './test-union';

export interface TestInterface {
  articleBody: string[];
  categorySpecificValues?: {[s: string]: string};
  inputType: 'multipleChoice';
  maintainer: TestThirdUnion | TestFirstEnum;
  remainingAttendeeCapacity?: number;
  test1: Array<TestThirdUnion | typeof TestFirstEnum>;
  test2: TestClass<string>;
  test3: 'test1' | 'test2';
  test4: TestSecondClass;
  universityRole: keyof TestFirstEnum;
}

export interface TestSecondInterface {
  [k: string]: string;
}
