/* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/no-empty-interface */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface Random {}

type TestArrayGeneric = Array<string>;
type TestArrayLiteral = number[];
type TestArrayReferenceGeneric = Array<Random>;
type TestArrayReferenceLiteral = Random[];

export const testConfig: EasyAstSpecType = {
  testName: `should resolve array-likes`,
  expected: {
    TestArrayGeneric: {
      name: 'TestArrayGeneric',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['type'],
      type: {
        value: 'string',
        flags: 4,
        isArray: true,
      },
    },
    TestArrayLiteral: {
      name: 'TestArrayLiteral',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['type'],
      type: {
        value: 'number',
        flags: 8,
        isArray: true,
      },
    },
    TestArrayReferenceGeneric: {
      name: 'TestArrayReferenceGeneric',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['type'],
      type: {
        referenceName: 'Random',
        flags: 524_288,
        isArray: true,
      },
    },
    TestArrayReferenceLiteral: {
      name: 'TestArrayReferenceLiteral',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['type'],
      type: {
        referenceName: 'Random',
        flags: 524_288,
        isArray: true,
      },
    },
    Random: {
      name: 'Random',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
    },
  },
};
