/* eslint-disable @typescript-eslint/no-empty-interface,@typescript-eslint/no-unused-vars */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface NestedObject {
  nested: {
    deeplyNested: {
      aNumber: number;
    };
  };
}

export const testConfig: EasyAstSpecType = {
  testName: `should handle nested/type literals`,
  expected: {
    NestedObject: {
      name: 'NestedObject',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      properties: {
        nested: {
          name: 'nested',
          type: {
            flags: 524_288,
          },
          properties: {
            deeplyNested: {
              name: 'deeplyNested',
              type: {
                flags: 524_288,
              },
              properties: {
                aNumber: {
                  name: 'aNumber',
                  type: {
                    flags: 8,
                    value: 'number',
                  },
                },
              },
            },
          },
        },
      },
    },
  },
};
