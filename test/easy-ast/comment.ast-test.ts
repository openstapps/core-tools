/* eslint-disable @typescript-eslint/no-unused-vars,jsdoc/check-tag-names */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

/**
 * Class comment
 *
 * Class description
 *
 * More description
 *
 * @classTag classParameter1 classParameter2
 */
interface TestInterface {
  /**
   * Property comment
   *
   * Property description
   *
   * More description
   *
   * @propertyTag propertyParameter1 propertyParameter2
   */
  foo: string;
}

/**
 * Class comment
 *
 * Class description
 *
 * More description
 *
 * @classTag classParameter1 classParameter2
 */
class TestClass {
  /**
   * Property comment
   *
   * Property description
   *
   * More description
   *
   * @propertyTag propertyParameter1 propertyParameter2
   */
  foo = 1;
}

/**
 * Enum comment
 *
 * Enum description
 *
 * More description
 *
 * @enumTag enumParameter1
 */
enum TestAlias {}

export const testConfig: EasyAstSpecType = {
  testName: `should resolve comments`,
  expected: {
    TestInterface: {
      comment: {
        shortSummary: 'Class comment',
        description: 'Class description\n\nMore description',
        tags: [
          {
            name: 'classTag',
            parameters: ['classParameter1', 'classParameter2'],
          },
        ],
      },
      name: 'TestInterface',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      properties: {
        foo: {
          comment: {
            shortSummary: 'Property comment',
            description: 'Property description\n\nMore description',
            tags: [
              {
                name: 'propertyTag',
                parameters: ['propertyParameter1', 'propertyParameter2'],
              },
            ],
          },
          name: 'foo',
          type: {
            value: 'string',
            flags: 4,
          },
        },
      },
    },
    TestClass: {
      comment: {
        shortSummary: 'Class comment',
        description: 'Class description\n\nMore description',
        tags: [
          {
            name: 'classTag',
            parameters: ['classParameter1', 'classParameter2'],
          },
        ],
      },
      name: 'TestClass',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['class'],
      properties: {
        foo: {
          comment: {
            shortSummary: 'Property comment',
            description: 'Property description\n\nMore description',
            tags: [
              {
                name: 'propertyTag',
                parameters: ['propertyParameter1', 'propertyParameter2'],
              },
            ],
          },
          name: 'foo',
          type: {
            value: 'number',
            flags: 8,
          },
        },
      },
    },
    TestAlias: {
      comment: {
        shortSummary: 'Enum comment',
        description: 'Enum description\n\nMore description',
        tags: [
          {
            name: 'enumTag',
            parameters: ['enumParameter1'],
          },
        ],
      },
      name: 'TestAlias',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['enum'],
    },
  },
};
