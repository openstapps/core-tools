/* eslint-disable @typescript-eslint/no-unused-vars */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

enum TestAuto {
  Foo,
  Bar,
}

enum TestSpecified {
  YES = 'yes',
  NO = 'no',
}

export const testConfig: EasyAstSpecType = {
  testName: `should resolve auto and specified enums`,
  expected: {
    TestAuto: {
      name: 'TestAuto',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['enum'],
      type: {
        flags: 1_048_576,
        specificationTypes: [
          {
            referenceName: 'Foo',
            value: 0,
            flags: 1280,
          },
          {
            referenceName: 'Bar',
            value: 1,
            flags: 1280,
          },
        ],
      },
    },
    TestSpecified: {
      name: 'TestSpecified',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['enum'],
      type: {
        flags: 1_048_576,
        specificationTypes: [
          {
            referenceName: 'YES',
            value: 'yes',
            flags: 1152,
          },
          {
            referenceName: 'NO',
            value: 'no',
            flags: 1152,
          },
        ],
      },
    },
  },
};
