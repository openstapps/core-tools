/* eslint-disable @typescript-eslint/no-unused-vars */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface Test1<T = number> {
  foo: T;
}

interface Test2 {
  bar: Test1;
}

export const testConfig: EasyAstSpecType = {
  testName: `should resolve default generics`,
  expected: {
    Test1: {
      name: 'Test1',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      typeParameters: ['T'],
      properties: {
        foo: {
          name: 'foo',
          type: {
            referenceName: 'T',
            flags: 262_144,
          },
        },
      },
    },
    Test2: {
      name: 'Test2',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      properties: {
        bar: {
          name: 'bar',
          type: {
            referenceName: 'Test1',
            flags: 524_288,
            genericsTypes: [
              {
                value: 'number',
                flags: 8,
              },
            ],
          },
        },
      },
    },
  },
};
