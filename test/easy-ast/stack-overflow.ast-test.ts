/* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/no-explicit-any */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface Foo<T extends Bar<string>> {
  bar: T;
}

interface Bar<T> {
  foo: T;
}

export const testConfig: EasyAstSpecType = {
  testName: `should ignore type constraints`,
  expected: {
    Foo: {
      name: 'Foo',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      typeParameters: ['T'],
      properties: {
        bar: {
          name: 'bar',
          type: {
            referenceName: 'T',
            flags: 262_144,
          },
        },
      },
    },
    Bar: {
      name: 'Bar',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      typeParameters: ['T'],
      properties: {
        foo: {
          name: 'foo',
          type: {
            referenceName: 'T',
            flags: 262_144,
          },
        },
      },
    },
  },
};
