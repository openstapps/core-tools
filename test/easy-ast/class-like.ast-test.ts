/* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/no-inferrable-types */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface TestInterface {
  foo: number;
}

class TestClass {
  bar: string = 'test';
}

export const testConfig: EasyAstSpecType = {
  testName: `should resolve class-likes`,
  expected: {
    TestInterface: {
      name: 'TestInterface',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      properties: {
        foo: {
          name: 'foo',
          type: {
            value: 'number',
            flags: 8,
          },
        },
      },
    },
    TestClass: {
      name: 'TestClass',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['class'],
      properties: {
        bar: {
          name: 'bar',
          type: {
            value: 'string',
            flags: 4,
          },
        },
      },
    },
  },
};
