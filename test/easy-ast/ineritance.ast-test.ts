/* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/no-inferrable-types */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface $BaseInterface<T> {
  foo: T;
}

interface $BaseInterface2 {
  bar: string;
}

class $BaseClass {}

class InheritingClass extends $BaseClass implements $BaseInterface<number>, $BaseInterface2 {
  bar: string = '';

  foo: number = 1;
}

export const testConfig: EasyAstSpecType = {
  testName: `inheritance`,
  expected: {
    InheritingClass: {
      name: 'InheritingClass',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['class'],
      implementedDefinitions: [
        {
          referenceName: '$BaseInterface',
          genericsTypes: [
            {
              value: 'number',
              flags: 8,
            },
          ],
          flags: 524_288,
        },
        {
          referenceName: '$BaseInterface2',
          flags: 524_288,
        },
      ],
      extendedDefinitions: [
        {
          referenceName: '$BaseClass',
          flags: 524_288,
        },
      ],
      properties: {
        foo: {
          name: 'foo',
          type: {
            value: 'number',
            flags: 8,
          },
        },
        bar: {
          name: 'bar',
          type: {
            value: 'string',
            flags: 4,
          },
        },
      },
    },
  },
};
