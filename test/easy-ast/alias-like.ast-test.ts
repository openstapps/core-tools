/* eslint-disable @typescript-eslint/no-unused-vars */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

type TestTypeAlias = number | string;

enum TestEnum {
  Foo,
  Bar,
}

export const testConfig: EasyAstSpecType = {
  testName: `should resolve alias-likes`,
  expected: {
    TestTypeAlias: {
      name: 'TestTypeAlias',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['type'],
      type: {
        flags: 1_048_576,
        specificationTypes: [
          {
            value: 'string',
            flags: 4,
          },
          {
            value: 'number',
            flags: 8,
          },
        ],
      },
    },
    TestEnum: {
      name: 'TestEnum',
      kind: LightweightDefinitionKind.ALIAS_LIKE,
      modifiers: ['enum'],
      type: {
        flags: 1_048_576,
        specificationTypes: [
          {
            referenceName: 'Foo',
            value: 0,
            flags: 1280,
          },
          {
            referenceName: 'Bar',
            value: 1,
            flags: 1280,
          },
        ],
      },
    },
  },
};
