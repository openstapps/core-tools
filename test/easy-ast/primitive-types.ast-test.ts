/* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/no-explicit-any */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface Test {
  number_type: number;
  string_type: string;
  boolean_type: boolean;
  any_type: any;
  unknown_type: unknown;
  null_type: null;
  undefined_type: undefined;
}

export const testConfig: EasyAstSpecType = {
  testName: `should interpret primitive types correctly`,
  expected: {
    Test: {
      name: 'Test',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      properties: {
        number_type: {
          name: 'number_type',
          type: {
            value: 'number',
            flags: 8,
          },
        },
        string_type: {
          name: 'string_type',
          type: {
            value: 'string',
            flags: 4,
          },
        },
        boolean_type: {
          name: 'boolean_type',
          type: {
            value: 'boolean',
            flags: 1_048_592,
            specificationTypes: [
              {
                value: 'false',
                flags: 512,
              },
              {
                value: 'true',
                flags: 512,
              },
            ],
          },
        },
        any_type: {
          name: 'any_type',
          type: {
            value: 'any',
            flags: 1,
          },
        },
        unknown_type: {
          name: 'unknown_type',
          type: {
            value: 'unknown',
            flags: 2,
          },
        },
        null_type: {
          name: 'null_type',
          type: {
            value: 'null',
            flags: 65_536,
          },
        },
        undefined_type: {
          name: 'undefined_type',
          type: {
            value: 'undefined',
            flags: 32_768,
          },
        },
      },
    },
  },
};
