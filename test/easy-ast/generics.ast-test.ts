/* eslint-disable @typescript-eslint/no-empty-interface,@typescript-eslint/no-unused-vars */
/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {EasyAstSpecType} from './easy-ast-spec-type';
import {LightweightDefinitionKind} from '../../src/easy-ast/types/lightweight-definition-kind';

interface $Random {}

interface Generics {
  baz: Foo<number, $Random>;
}

interface Foo<T, S> {
  foo: T;
  bar: S;
}

export const testConfig: EasyAstSpecType = {
  testName: `should resolve generics`,
  expected: {
    Generics: {
      name: 'Generics',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      properties: {
        baz: {
          name: 'baz',
          type: {
            referenceName: 'Foo',
            flags: 524_288,
            genericsTypes: [
              {
                value: 'number',
                flags: 8,
              },
              {
                referenceName: '$Random',
                flags: 524_288,
              },
            ],
          },
        },
      },
    },
    Foo: {
      name: 'Foo',
      kind: LightweightDefinitionKind.CLASS_LIKE,
      modifiers: ['interface'],
      typeParameters: ['T', 'S'],
      properties: {
        foo: {
          name: 'foo',
          type: {
            referenceName: 'T',
            flags: 262_144,
          },
        },
        bar: {
          name: 'bar',
          type: {
            referenceName: 'S',
            flags: 262_144,
          },
        },
      },
    },
  },
};
