# [0.34.0](https://gitlab.com/openstapps/core-tools/compare/v0.33.0...v0.34.0) (2023-01-30)



# [0.33.0](https://gitlab.com/openstapps/core-tools/compare/v0.32.0...v0.33.0) (2023-01-12)


### Bug Fixes

* convert schema glob path to posix path ([c3d66b3](https://gitlab.com/openstapps/core-tools/commit/c3d66b36c8b4bdf84b3e23cd0ab8318d3fd068f9))



# [0.32.0](https://gitlab.com/openstapps/core-tools/compare/v0.31.0...v0.32.0) (2022-08-17)


### Bug Fixes

* documentation not beeing generated ([51ae246](https://gitlab.com/openstapps/core-tools/commit/51ae246a0fd10144cf6af348bc7ded88e6dab27a))



# [0.31.0](https://gitlab.com/openstapps/core-tools/compare/v0.30.1...v0.31.0) (2022-06-27)



## [0.30.1](https://gitlab.com/openstapps/core-tools/compare/v0.30.0...v0.30.1) (2022-05-27)



# [0.30.0](https://gitlab.com/openstapps/core-tools/compare/v0.29.1...v0.30.0) (2022-04-04)



## [0.29.1](https://gitlab.com/openstapps/core-tools/compare/v0.29.0...v0.29.1) (2022-03-21)



# [0.29.0](https://gitlab.com/openstapps/core-tools/compare/v0.28.0...v0.29.0) (2022-01-25)



# [0.28.0](https://gitlab.com/openstapps/core-tools/compare/v0.27.0...v0.28.0) (2021-12-17)


### Bug Fixes

* ajv related tranistive build errors ([e82f4e9](https://gitlab.com/openstapps/core-tools/commit/e82f4e92099359baed5ea89f64bd7204700c2785))



# [0.27.0](https://gitlab.com/openstapps/core-tools/compare/v0.26.0...v0.27.0) (2021-12-15)



# [0.26.0](https://gitlab.com/openstapps/core-tools/compare/v0.25.0...v0.26.0) (2021-12-15)



# [0.25.0](https://gitlab.com/openstapps/core-tools/compare/v0.24.3...v0.25.0) (2021-08-31)


### Bug Fixes

* generate schemas for type aliases ([96e3acf](https://gitlab.com/openstapps/core-tools/commit/96e3acf9ba330c3414eb1e38b347ca9e597fee88))



## [0.24.3](https://gitlab.com/openstapps/core-tools/compare/v0.24.2...v0.24.3) (2021-08-27)


### Bug Fixes

* add cruical Converter ceation option ([f82b465](https://gitlab.com/openstapps/core-tools/commit/f82b4652637808abf68f1e72b1de0c76b228457a))



## [0.24.2](https://gitlab.com/openstapps/core-tools/compare/v0.24.1...v0.24.2) (2021-08-25)



## [0.24.1](https://gitlab.com/openstapps/core-tools/compare/v0.24.0...v0.24.1) (2021-08-25)



# [0.24.0](https://gitlab.com/openstapps/core-tools/compare/v0.23.2...v0.24.0) (2021-08-25)


### Features

* modernize core-tools ([fe59204](https://gitlab.com/openstapps/core-tools/commit/fe59204b4210831b15445fa9aa7dbc20de75e96d))



## [0.23.2](https://gitlab.com/openstapps/core-tools/compare/v0.23.0...v0.23.2) (2021-08-02)


### Bug Fixes

* rollback ajv version to 6.12.6 ([7a5d2bb](https://gitlab.com/openstapps/core-tools/commit/7a5d2bb1e98b41d1143fbfe21b29e3aef2133c8f))



# [0.23.0](https://gitlab.com/openstapps/core-tools/compare/v0.22.0...v0.23.0) (2021-07-21)


### Features

* replace route markdown with openapi ([f4bf7ab](https://gitlab.com/openstapps/core-tools/commit/f4bf7abc895f87a57fa34b2269311809f2a9413d))



# [0.22.0](https://gitlab.com/openstapps/core-tools/compare/v0.21.0...v0.22.0) (2021-07-07)


### Bug Fixes

* fix inherited properties not working correctly ([f20038c](https://gitlab.com/openstapps/core-tools/commit/f20038c8f9a37424fd7a95484e744e0c672f5cfb))



# [0.21.0](https://gitlab.com/openstapps/core-tools/compare/v0.20.0...v0.21.0) (2021-05-12)


### Features

* add support for non-external premaps ([7429806](https://gitlab.com/openstapps/core-tools/commit/74298065e0386c8e4646e565e5e383b5ae08dfaa))



# [0.20.0](https://gitlab.com/openstapps/core-tools/compare/v0.19.0...v0.20.0) (2021-04-26)


### Features

* add filterable annotation option for integer ([7ba8233](https://gitlab.com/openstapps/core-tools/commit/7ba8233f5b81f4ab3c3aeec2c8a47a7bad3ae502))



# [0.19.0](https://gitlab.com/openstapps/core-tools/compare/v0.18.0...v0.19.0) (2021-04-06)



# [0.18.0](https://gitlab.com/openstapps/core-tools/compare/v0.17.0...v0.18.0) (2021-03-24)


### Features

* make Point type sortable in Elasticsearch ([724a686](https://gitlab.com/openstapps/core-tools/commit/724a6866c80a544dec4ce11d70d648bd724f9aba))



# [0.17.0](https://gitlab.com/openstapps/core-tools/compare/v0.16.0...v0.17.0) (2020-12-02)


### Features

* add support for [@inherit](https://gitlab.com/inherit)Tags ([485430b](https://gitlab.com/openstapps/core-tools/commit/485430b7f27fb9c751a6f5697e74eb5531ac7889))
* add support for date mapping ([a09be1d](https://gitlab.com/openstapps/core-tools/commit/a09be1d941df88826ccaa8aa468680ece29d35a5))



# [0.16.0](https://gitlab.com/openstapps/core-tools/compare/v0.15.0...v0.16.0) (2020-10-28)


### Bug Fixes

* use value type in IndexSignature type instead of key type ([b7cdb6a](https://gitlab.com/openstapps/core-tools/commit/b7cdb6a9ad2bdb1fc09d0f535daf4ec778501fec))



# [0.15.0](https://gitlab.com/openstapps/core-tools/compare/v0.14.0...v0.15.0) (2020-10-05)


### Bug Fixes

* array tags did not propagate ([e5511d0](https://gitlab.com/openstapps/core-tools/commit/e5511d07386e93b032b5dbefa3e6cc46c07219ed))



# [0.14.0](https://gitlab.com/openstapps/core-tools/compare/v0.13.0...v0.14.0) (2020-02-11)



# [0.13.0](https://gitlab.com/openstapps/core-tools/compare/v0.12.0...v0.13.0) (2020-01-28)



# [0.12.0](https://gitlab.com/openstapps/core-tools/compare/v0.11.0...v0.12.0) (2019-12-03)


### Bug Fixes

* make fields with index signatures a dynamic mapping ([8f5570e](https://gitlab.com/openstapps/core-tools/commit/8f5570e2e2d350d6153b34f940f4cd5c407b5bc9))



# [0.11.0](https://gitlab.com/openstapps/core-tools/compare/v0.10.0...v0.11.0) (2019-11-12)


### Bug Fixes

* use .raw field for aggregations ([4c1a374](https://gitlab.com/openstapps/core-tools/commit/4c1a374a9ee266884bd33e8a3b3f52fcef8f4019))



# [0.10.0](https://gitlab.com/openstapps/core-tools/compare/v0.9.0...v0.10.0) (2019-11-08)


### Bug Fixes

* make mapping of generics work correctly ([8f7201e](https://gitlab.com/openstapps/core-tools/commit/8f7201e2cf010ed9ef457d9cf938167a4e970963)), closes [#27](https://gitlab.com/openstapps/core-tools/issues/27)
* make mapping tags work for overwritten values ([47361d4](https://gitlab.com/openstapps/core-tools/commit/47361d412a92ae34dc1b81298402d6b441b4f4ba))


### Features

* generate aggreations from annotations in the core ([18ad651](https://gitlab.com/openstapps/core-tools/commit/18ad651286bafcbc929e14ee9429c7b852d86f3b))



# [0.9.0](https://gitlab.com/openstapps/core-tools/compare/v0.8.0...v0.9.0) (2019-09-10)


### Bug Fixes

* read type tags correctly after the first time ([77e4914](https://gitlab.com/openstapps/core-tools/commit/77e49146c0619566919815bd5d63ddf34dc19387))


### Features

* add support for [@filterable](https://gitlab.com/filterable) tag ([36bf17e](https://gitlab.com/openstapps/core-tools/commit/36bf17e3236a555e6d1193466141be880b280db9))



# [0.8.0](https://gitlab.com/openstapps/core-tools/compare/v0.7.0...v0.8.0) (2019-07-25)


### Bug Fixes

* apply stricter tslint rules ([967f946](https://gitlab.com/openstapps/core-tools/commit/967f94652723d8589b7f272454185449233ac838))
* emend the imports in the test files ([53e8047](https://gitlab.com/openstapps/core-tools/commit/53e80476c85027194cf406ad87fe8950d6c2f3a8))
* remove duplicate job ([af904a7](https://gitlab.com/openstapps/core-tools/commit/af904a7a05de56cdef6aea136a7e3bb237c4beb2))
* update the uml job to use our node image ([a478715](https://gitlab.com/openstapps/core-tools/commit/a478715d8045dd26cd345ba3cbb469fda651b84f))


### Features

* add automatic mapping generation ([7b198f9](https://gitlab.com/openstapps/core-tools/commit/7b198f95ce7bbd75454247e70757d34af462cefc)), closes [#6](https://gitlab.com/openstapps/core-tools/issues/6)
* add the uml generator ([0f21da4](https://gitlab.com/openstapps/core-tools/commit/0f21da4a92b4e0ef11a5f274468d3679fc9784ee))
* added output file name for uml generation ([843e598](https://gitlab.com/openstapps/core-tools/commit/843e59811a5a104df1c746627aa668d26fdc9f60))



# [0.7.0](https://gitlab.com/openstapps/core-tools/compare/v0.6.0...v0.7.0) (2019-06-24)



# [0.6.0](https://gitlab.com/openstapps/core-tools/compare/v0.5.1...v0.6.0) (2019-04-16)



## [0.5.1](https://gitlab.com/openstapps/core-tools/compare/v0.5.0...v0.5.1) (2019-04-09)



# [0.5.0](https://gitlab.com/openstapps/core-tools/compare/v0.4.0...v0.5.0) (2019-04-09)



# [0.4.0](https://gitlab.com/openstapps/core-tools/compare/v0.3.0...v0.4.0) (2019-04-03)


### Features

* add feature to validate schemas directly ([1022150](https://gitlab.com/openstapps/core-tools/commit/1022150ca3d2af846b819e0e0e46ead71134d5f8))
* adjust generation of route documentation ([d3ce936](https://gitlab.com/openstapps/core-tools/commit/d3ce936626751f24f20081403271d77e2c346e03)), closes [#12](https://gitlab.com/openstapps/core-tools/issues/12)



# [0.3.0](https://gitlab.com/openstapps/core-tools/compare/v0.2.1...v0.3.0) (2019-02-06)


### Features

* ensure correct path for input files ([1c74432](https://gitlab.com/openstapps/core-tools/commit/1c744328eb03dc8019fe6dc3a309f68260210146))



## [0.2.1](https://gitlab.com/openstapps/core-tools/compare/v0.2.0...v0.2.1) (2019-01-30)


### Bug Fixes

* add SC prefix to the camel cased type of the instance ([e559234](https://gitlab.com/openstapps/core-tools/commit/e559234cea9ef245733f8117ab4a27e83db54e14))
* use tsconfig.json of project for schema generation ([6b1a420](https://gitlab.com/openstapps/core-tools/commit/6b1a4202f92759c9ef36d32d0960faf4f56cc8db)), closes [#10](https://gitlab.com/openstapps/core-tools/issues/10)


### Features

* add function to find tsconfig.json ([aa645a2](https://gitlab.com/openstapps/core-tools/commit/aa645a2cc4661c58c6050196ee1944dfd9e5eea8))



# [0.2.0](https://gitlab.com/openstapps/core-tools/compare/v0.1.1...v0.2.0) (2019-01-21)


### Features

* add pack script ([7438465](https://gitlab.com/openstapps/core-tools/commit/7438465149b591ebf1583a8bd6b9da56aa9c9f57)), closes [#4](https://gitlab.com/openstapps/core-tools/issues/4)



## [0.1.1](https://gitlab.com/openstapps/core-tools/compare/v0.1.0...v0.1.1) (2019-01-14)


### Bug Fixes

* add missing dependency typedoc ([b248d1b](https://gitlab.com/openstapps/core-tools/commit/b248d1b5e0306247ce35e5c9d45637b0f3f83cac)), closes [#5](https://gitlab.com/openstapps/core-tools/issues/5)



# [0.1.0](https://gitlab.com/openstapps/core-tools/compare/v0.0.3...v0.1.0) (2019-01-08)


### Features

* validate generated schemas ([7b7299d](https://gitlab.com/openstapps/core-tools/commit/7b7299d9c475105f9d2387c9dc31974139997483)), closes [#1](https://gitlab.com/openstapps/core-tools/issues/1)



## [0.0.3](https://gitlab.com/openstapps/core-tools/compare/v0.0.2...v0.0.3) (2018-12-18)


### Bug Fixes

* do not ignore resources ([6502bcf](https://gitlab.com/openstapps/core-tools/commit/6502bcf2b517ab98909626a7b8baa08e7cdcdc3e))



## [0.0.2](https://gitlab.com/openstapps/core-tools/compare/v0.0.1...v0.0.2) (2018-12-18)



## [0.0.1](https://gitlab.com/openstapps/core-tools/compare/1ac90ef6330bcfd0ea362505a11db66453f3386f...v0.0.1) (2018-12-18)


### Features

* add core tools ([1ac90ef](https://gitlab.com/openstapps/core-tools/commit/1ac90ef6330bcfd0ea362505a11db66453f3386f))



