# @openstapps/core-tools

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/core-tools.svg?style=flat-square)](https://gitlab.com/openstapps/core-tools/commits/master)
[![npm](https://img.shields.io/npm/v/@openstapps/core-tools.svg?style=flat-square)](https://npmjs.com/package/@openstapps/core-tools)
[![license)](https://img.shields.io/npm/l/@openstapps/core-tools.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://openstapps.gitlab.io/core-tools)

Tools to convert and validate StAppsCore

## What are the tools for?

The StAppsCore Converter is a tool for converting SC-types (TypeScript) into JSON schema files.

JSON schema files are needed for run-time validation of SC-type objects, as this is a tedious task to do using SC-types defined in TypeScript (not possible without additional coding). That said, StAppsCore Converter practically prepares SC-types to be used for object validation (determining whether a JavaScript/JSON object is a valid object of the corresponding SC-type) using StAppsCore Validator.

The StAppsCore Validator is a tool for run-time validation of objects (determining whether a JavaScript/JSON object is a valid object of the corresponding SC-type. It consumes JSON schema files from StAppsCore as the definitions of SC-types against which are validated concrete (actual) objects (as an example SCDish object in the example below).

## Installation

Installation of the npm package (using `npm install`) makes the tool available as an executable with the name `openstapps-core-tools`.

## How to use the converter?

Add `@validatable` to the Typedoc comment of the types that you want to convert to JSONSchema.

The command `openstapps-core-tools` can then be called using these arguments:

```shell
openstapps-core-tools schema <srcPath> <schemaPath>
```

where:
- `<srcPath>` is path to the project (where used `*.ts` files are, e.g. `src/core`,
- `<schemaPath>` is directory to save output files to, e.g. `lib/schema`.

Complete command with the example arguments is then:

```shell
openstapps-core-tools schema src/core lib/schema
```

Inside of a script in `package.json` or if the npm package is installed globally, the tool `stapps-convert` can be called without its local path (`node_modules/.bin`):

```shell
openstapps-core-tools schema src/core lib/schema
```

## What is Easy AST?

Easy [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree) is a "wrapper" around the TypeScript Compiler API. The TS Compiler API is built for many
much more complex things than just accessing type declarations, and thus comes with a ton of
bloat that makes it really difficult to work with.

This is why we built the Easy AST, which converts the TS representation to a really easy and
lightweight structure, that removes a lot of the stuff you wouldn't really need.

## How to use the validator?

### Using the validator programatically

```typescript
import {Validator} from '@openstapps/core-tools/lib/validate';
import {SCDish, SCThingType} from '@openstapps/core';
import {ValidatorResult} from 'jsonschema';
import {join} from 'path';

const objectToValidate: SCDish = {
type: SCThingType.Dish,
  // more properties
};

// instantiate a new validator
const validator = new Validator();

// make the validator read the schema files
validator.addSchemas(join('node_modules', '@openstapps', 'core', 'lib', 'schema')).then(() => {
  // validate an object
  const result: ValidatorResult = validator.validate(objectToValidate, 'SCDish');
});
```

#### Using validateFiles function

The JSON files passed to the validateFiles method have an added layer.
That layer encapsulates the actual JSON data of the object to be verified and adds a property to enable true negative testing.

Your basic JSON object:

```json
{
  "property1": "value1",
  "property2": "value2",
  ...
}
```

JSON for validateFiles:
```json
{
  "errorNames": [],
  "instance": {
    "property1": "value1",
    "property2": "value2",
    ...
  },
  "schema": "NameOfSchema"
}
```

Where `errorNames` holds the string values of the name property of the expected ValidationErrors from JSON Schema. Empty array means no errors are expected.

`schema` holds the name of the schema to validate the instance against.

### How to use validator as a CLI tool (executable)?

The command `openstapps-core-tools` can then be called using these arguments:

```shell
openstapps-core-tools validate <schemaPath> <testPath> [reportPath]
```

where:
- `<schemaPath>` is a directory where JSON schema files are, e.g. `lib/schema`,
- `<testPath>` is a directory where test files are, e.g. `src/test/resources`,
- `[reportPath]` is a file where the HTML report of the validation will be saved to, e.g. `report.html` (optional argument - if it's not provided no report will be written).

Command with the example arguments is then for example:

```shell
openstapps-core-tools validate lib/schema src/test/resources
```

Inside of a script in `package.json` or if the npm package is installed globally, the tool `openstapps-validate` can be called without its local path (`node_modules/.bin`):

```shell
openstapps-core-tools validate lib/schema src/test/resources report.html
```

## Generate openapi JSON file for routes

To generate a openapi JSON file that represents the routes according to openapi version 3.0.3 use the following command.

```shell
openstapps-core-tools openapi PATH/TO/CORE/lib PATH/TO/PUT/FILES/TO
```

## Pack definitions and implementations

To pack all the different files into two distribution files - one for definitions/one for implementations - use the following command:

```shell
openstapps-core-tools pack
```

## How to use the UML generator

The UML Generator generates PlantUML from the project reflection of the source files. By default it will include externals, which will take considerably longer to execute, you can disable this behaviour via an option. It can help you to visually explore the data model or document a specific part.

You can either use the public PlantUML-server or start your own local instance. To run, restart or stop the container use the scripts provided in the `package.json`.

### Generating from source-files

```shell
openstapps-core-tools plantuml PATH/TO/SOURCEFILES http://PLANTUMLSERVER
```

Executing this command will generate a `.svg` file in your current working directory.

Multiple options can be set to enhance the diagram. By default all additional information other than the definitions are disabled. You can use:

- `--showProperties` to show all mandatory attributes of the classes and interfaces.
- `--showOptionalProperties` to show all mandatory attributes of the classes and interfaces. `--showProperties` must be set!
- `--showInheritedProperties` to show all inherited attributes of the classes and interfaces. `--showProperties` must be set!
- `--showEnumValues` to show all enumeration and type (enumeration-like) values
- `--showInheritance` to show the hierarchy of the classes and interfaces. Inherited attributes will only be shown in their parent.
- `--showAssociations` to show all references of classes and interfaces between one another
- `--excludeExternals` to exclude external definitions
- `--definitions <definitons>` to show only specific definitions to reduce the output of the diagram. `<definitions>` is a comma seperated list of definitions.
- `--outputFileName <fileName>` for a custom file name, the file extension will be added automatically (.svg). Otherwise a generic file with a timestamp will be generated into the execution directory. If a file with the same name already exists it will be overwritten!

The best way to explore models is to enable `--showInheritance` and `--showAssociations`. Start with just one definition in your `--definition <definitions>`-list, generate the diagram, look at it, add a new definition that you have seen to your command and generate anew.

#### Examples

Show the class hierarchy of the whole project:

```shell
openstapps-core-tools plantuml PATH/TO/SRCDIR http://PLANTUMLSERVER --showInheritance
```

Show the dish-module:

```shell
openstapps-core-tools plantuml ../core http://localhost:8080 --showProperties --showOptionalProperties --showInheritance --showAssociations --showEnumValues --definitions SCDish,SCThingThatCanBeOfferedOffer
```

### Generating from existing file

The plantuml code is persisted inside the generated file at the very bottom. You can tweak the model by using the function to generate UML from a PlantUML-file(simple text file). Extract the code (starting from `@startuml` to `@enduml`), edit it manually and execute this function.

```shell
openstapps-core-tools plantuml-file /PATH/TO/Project.plantuml http://PLANTUMLSERVER OptionalCustomFileName
```

Example-File-Content of Project.plantuml 
```
@startuml
interface MyClass{
  myProperty: string
}
@enduml
```
